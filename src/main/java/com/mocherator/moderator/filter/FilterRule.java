package com.mocherator.moderator.filter;

import com.mocherator.telegram.dto.MessageDTO;

public interface FilterRule {
    boolean isShouldBeRemoved(MessageDTO message);
}
