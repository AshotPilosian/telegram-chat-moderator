package com.mocherator.moderator.filter;

import com.mocherator.telegram.dto.MessageDTO;

import java.util.concurrent.CopyOnWriteArrayList;

public class StickerMessageFilterRule implements FilterRule {
    private final CopyOnWriteArrayList<String> exceptUsers;

    public StickerMessageFilterRule() {
        this.exceptUsers = new CopyOnWriteArrayList<>();
    }

    public void addExceptUser(String userName) {
        exceptUsers.add(userName);
    }

    public void removeExceptUser(String userName) {
        exceptUsers.remove(userName);
    }

    public CopyOnWriteArrayList<String> getExceptUsers() {
        return exceptUsers;
    }

    @Override
    public boolean isShouldBeRemoved(MessageDTO message) {
        if (message.getSticker() != null) {
            if (exceptUsers.isEmpty()) {
                return true;
            } else {
                if (message.getFrom() != null && message.getFrom().getUsername() != null) {
                    return !exceptUsers.contains(message.getFrom().getUsername());
                }
            }
        }

        return false;
    }

    @Override
    public String toString() {
        return "StickerMessageFilterRule{" +
                "exceptUsers=" + exceptUsers +
                '}';
    }
}
