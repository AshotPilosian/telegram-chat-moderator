package com.mocherator.moderator;

import com.github.badoualy.telegram.tl.exception.RpcErrorException;
import com.mocherator.moderator.filter.StickerMessageFilterRule;
import com.mocherator.telegram.client.TelegramBotClient;
import com.mocherator.telegram.client.TelegramMProtoClient;
import com.mocherator.telegram.data.ChatInfo;
import com.mocherator.telegram.data.ModerateChatInfo;
import one.util.streamex.StreamEx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class Moderator {
    private static final Logger log = LoggerFactory.getLogger(Moderator.class);

    private final String configRootDir;

    private final TelegramMProtoClient mProtoClient;
    private final TelegramBotClient botClient;

    private final ConcurrentMap<Integer, ModerateChatInfo> chatsToModerate;

    private Thread workerThread;
    private final AtomicBoolean isRunning;

    public Moderator(String configRootDir,
                     String mProtoApiHash, int mProtoApiId, String mProtoPhoneNumber,
                     String botToken) {
        this.configRootDir = configRootDir;
        this.mProtoClient = new TelegramMProtoClient(configRootDir, mProtoApiHash, mProtoApiId, mProtoPhoneNumber);
        this.botClient = new TelegramBotClient(botToken);
        this.isRunning = new AtomicBoolean(false);
        this.chatsToModerate = new ConcurrentHashMap<>();
    }

    public void start() {
        workerThread = new Thread(new ModeratorWorker(isRunning, mProtoClient, botClient, chatsToModerate, this));
        isRunning.set(true);
        botClient.startReceivingUpdates();
        workerThread.start();
    }

    public void stop() {
        isRunning.set(false);
        workerThread.interrupt();
        try {
            workerThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        workerThread = null;
    }

    public void loadModerationConfig() {
        File moderationConfig = new File(configRootDir, "moderationConfig");
        if (!moderationConfig.exists()) {
            log.info("Moderation config doesn't exists: " + moderationConfig.getPath());
            log.debug("Moderation config doesn't exists: " + moderationConfig.getPath());
            return;
        }

        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(new FileInputStream(moderationConfig), "UTF-8"))) {
            br.lines()
                    .map(line -> line.split(":"))
                    .forEach(splittedLine -> {
                        String title = splittedLine[0];
                        Optional<ModerateChatInfo> moderateChatInfo = addModeratedChat(title);
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        if (splittedLine.length > 1) {
                            String exclusions = splittedLine[1];
                            if (!exclusions.trim().isEmpty()) {
                                moderateChatInfo.ifPresent(mci -> {
                                    StreamEx.of(mci.getFilterRules())
                                            .select(StickerMessageFilterRule.class)
                                            .forEach(rule -> Arrays.stream(exclusions.split(","))
                                                    .forEach(rule::addExceptUser)
                                            );
                                });
                            }
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }

        log.debug(buildModerationListInfo());
    }

    public void storeModerationConfig() {
        File moderationConfig = new File(configRootDir, "moderationConfig");
        if (!moderationConfig.exists()) {
            log.debug("Moderation config doesn't exists: " + moderationConfig.getPath());
            try {
                if (moderationConfig.createNewFile()) {
                    log.debug("Created moderation config: " + moderationConfig.getPath());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        List<String> moderationConfigLines = chatsToModerate.values().stream()
                .map(moderateChatInfo -> {
                    String title = moderateChatInfo.getChatInfo().getTitle();
                    String exclusions = StreamEx.of(moderateChatInfo.getFilterRules())
                            .select(StickerMessageFilterRule.class)
                            .flatMap(rule -> rule.getExceptUsers().stream())
                            .distinct()
                            .collect(Collectors.joining(","));
                    return title + ":" + exclusions;
                })
                .collect(Collectors.toList());

        try (PrintWriter pw = new PrintWriter(new OutputStreamWriter(
                new FileOutputStream(moderationConfig), "UTF-8"))) {
            moderationConfigLines.forEach(pw::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String buildModerationListInfo() {
        return chatsToModerate.values().stream()
                .map(moderateChatInfo -> {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Title: ")
                            .append(moderateChatInfo.getChatInfo().getTitle())
                            .append("; ")
                            .append("Exclusions: ")
                            .append(moderateChatInfo.getFilterRules());
                    return sb.toString();
                })
                .collect(Collectors.joining("\n"));
    }

    private Optional<ModerateChatInfo> addModeratedChat(String chatName) {
        try {
            Optional<ChatInfo> chatInfo = mProtoClient.findChat(chatName);
            if (!chatInfo.isPresent()) {
                log.debug("Can't find chat: " + chatName);
                return Optional.empty();
            }

            if (!chatInfo.get().isMocheratorAdmin()) {
                log.debug("Mocherator is not an admin in chat: " + chatName);
                return Optional.empty();
            }

            ModerateChatInfo moderateChatInfo = new ModerateChatInfo(chatInfo.get(), new StickerMessageFilterRule());
            chatsToModerate.put(chatInfo.get().getId(), moderateChatInfo);
            return Optional.of(moderateChatInfo);
        } catch (IOException | RpcErrorException e) {
            log.debug("Error occurred while trying to load info about chat: " + chatName);

            return Optional.empty();
        }
    }
}
