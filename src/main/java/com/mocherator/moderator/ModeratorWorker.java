package com.mocherator.moderator;

import com.github.badoualy.telegram.tl.exception.RpcErrorException;
import com.mocherator.moderator.filter.FilterRule;
import com.mocherator.moderator.filter.StickerMessageFilterRule;
import com.mocherator.telegram.client.TelegramBotClient;
import com.mocherator.telegram.client.TelegramMProtoClient;
import com.mocherator.telegram.data.ChatInfo;
import com.mocherator.telegram.data.ModerateChatInfo;
import com.mocherator.telegram.dto.MessageDTO;
import com.mocherator.telegram.dto.UpdateDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

class ModeratorWorker implements Runnable {
    private static final Logger log = LoggerFactory.getLogger(ModeratorWorker.class);

    private final AtomicBoolean isRunning;
    private final TelegramMProtoClient mProtoClient;
    private final TelegramBotClient botClient;
    private final ConcurrentMap<Integer, ModerateChatInfo> chatsToModerate;
    private final Moderator moderator;

    public ModeratorWorker(AtomicBoolean isRunning,
                           TelegramMProtoClient mProtoClient,
                           TelegramBotClient botClient,
                           ConcurrentMap<Integer, ModerateChatInfo> chatsToModerate,
                           Moderator moderator) {
        this.isRunning = isRunning;
        this.mProtoClient = mProtoClient;
        this.botClient = botClient;
        this.chatsToModerate = chatsToModerate;
        this.moderator = moderator;
    }

    @Override
    public void run() {
        BlockingQueue<UpdateDTO> updatesQueue = botClient.getUpdatesQueue();

        while (Thread.currentThread().isAlive() && isRunning.get()) {
            List<UpdateDTO> updatesToProcess = new ArrayList<>();
            try {
                while (true) {
                    log.debug("Waiting for updates.");
                    updatesToProcess.add(updatesQueue.take());
                    // TODO: Rewrite this shit. May be cause of long check pauses.
                    Thread.sleep(300);
                    while (!updatesQueue.isEmpty() && updatesToProcess.size() < 5) {
                        Thread.sleep(500);
                        UpdateDTO updateDTO = updatesQueue.poll();
                        if (updateDTO != null) {
                            updatesToProcess.add(updateDTO);
                        }
                    }

                    log.debug("Received updates: " + updatesToProcess.size());
                    List<UpdateDTO> validUpdates = updatesToProcess.stream()
                            .filter(update -> update.getMessage() != null)
                            .filter(update -> update.getMessage().getChat() != null)
                            .filter(update -> update.getMessage().getChat().getId() != null)
                            .collect(Collectors.toList());

                    List<MessageDTO> adminMessages = validUpdates.stream()
                            .map(UpdateDTO::getMessage)
                            .filter(msg -> "APilosian".equals(msg.getChat().getUsername()))
                            .filter(msg -> msg.getText().startsWith("/"))
                            .collect(Collectors.toList());

                    processAdminMessages(adminMessages);

                    Map<Integer, List<MessageDTO>> messagesToRemove = validUpdates.stream()
                            .filter(update -> chatsToModerate.containsKey(update.getMessage().getChat().getId()))
                            .filter(update -> {
                                CopyOnWriteArrayList<FilterRule> filterRules =
                                        chatsToModerate.get(update.getMessage().getChat().getId()).getFilterRules();
                                for (FilterRule filterRule : filterRules) {
                                    if (filterRule.isShouldBeRemoved(update.getMessage())) {
                                        return true;
                                    }
                                }

                                return false;
                            })
                            .map(UpdateDTO::getMessage)
                            .collect(Collectors.groupingBy(msg -> msg.getChat().getId()));

                    log.debug("Messages to remove: " + messagesToRemove.values().stream()
                            .mapToLong(Collection::size)
                            .sum());

                    for (Map.Entry<Integer, List<MessageDTO>> pair : messagesToRemove.entrySet()) {
                        try {
                            mProtoClient.deleteMessages(pair.getValue(),
                                    chatsToModerate.get(pair.getKey()).getChatInfo());
                        } catch (IOException | RpcErrorException e) {
                            e.printStackTrace();
                        }
                    }

                    updatesToProcess.clear();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void processAdminMessages(List<MessageDTO> adminMessages) {
        for (MessageDTO adminMessage : adminMessages) {
            String[] msgParts = adminMessage.getText().split(":");
            if (msgParts[0].equals("/add_exclusion")){
                processAddExclusionCommand(adminMessage);
            } else if (msgParts[0].equals("/remove_exclusion")){
                processRemoveExclusionCommand(adminMessage);
            } else if (msgParts[0].equals("/enable_moderation")){
                processEnableModerationCommand(adminMessage);
            } else if (msgParts[0].equals("/disable_moderation")){
                processDisableModerationCommand(adminMessage);
            } else if (msgParts[0].equals("/list")){
                processListCommand(adminMessage);
            } else {
                botClient.sendMessage(adminMessage.getChat().getId(), "ERROR: Unknown command.");
            }
        }

        moderator.storeModerationConfig();
    }

    private void processAddExclusionCommand(MessageDTO messageDTO) {
        String[] msgParts = messageDTO.getText().split(":");

        if (msgParts.length != 3) {
            log.debug("Not enough command parts.");
            botClient.sendMessage(messageDTO.getFrom().getId(),
                    "ERROR: Not enough command parts. Usage: /addExclusion:chat title:username");
            return;
        }

        String chatName = msgParts[1];
        String userName = msgParts[2];

        try {
            Optional<ChatInfo> chat = mProtoClient.findChat(chatName);
            if (!chat.isPresent()) {
                log.debug("Can't find chat with name: " + chatName);
                botClient.sendMessage(messageDTO.getFrom().getId(),
                        "ERROR: Can't find chat with name: " + chatName);
                return;
            }

            ModerateChatInfo moderateChatInfo = chatsToModerate.get(chat.get().getId());
            if (moderateChatInfo == null) {
                log.debug("Don't moderating this chat: " + chatName);
                botClient.sendMessage(messageDTO.getFrom().getId(),
                        "ERROR: Don't moderating this chat: " + chatName);
                return;
            }

            for (FilterRule filterRule : moderateChatInfo.getFilterRules()) {
                if (filterRule instanceof StickerMessageFilterRule) {
                    StickerMessageFilterRule stickerMessageFilterRule = (StickerMessageFilterRule) filterRule;
                    stickerMessageFilterRule.addExceptUser(userName);
                    log.debug("Added to exclusions. User: " + userName + "; Chat: " + chatName);
                    botClient.sendMessage(messageDTO.getFrom().getId(),
                            "Added to exclusions. User: " + userName + "; Chat: " + chatName);
                }
            }
        } catch (IOException | RpcErrorException e) {
            e.printStackTrace();
        }
    }

    private void processRemoveExclusionCommand(MessageDTO messageDTO) {
        String[] msgParts = messageDTO.getText().split(":");

        if (msgParts.length != 3) {
            log.debug("Not enough command parts. Usage: /removeExclusion:chat title:username");
            botClient.sendMessage(messageDTO.getFrom().getId(),
                    "ERROR: Not enough command parts. Usage: /removeExclusion:chat title:username");
            return;
        }

        String chatName = msgParts[1];
        String userName = msgParts[2];

        try {
            Optional<ChatInfo> chat = mProtoClient.findChat(chatName);
            if (!chat.isPresent()) {
                log.debug("Can't find chat with name: " + chatName);
                botClient.sendMessage(messageDTO.getFrom().getId(),
                        "ERROR: Can't find chat with name: " + chatName);
                return;
            }

            ModerateChatInfo moderateChatInfo = chatsToModerate.get(chat.get().getId());
            if (moderateChatInfo == null) {
                log.debug("Don't moderating this chat: " + chatName);
                botClient.sendMessage(messageDTO.getFrom().getId(),
                        "ERROR: Don't moderating this chat: " + chatName);
                return;
            }

            for (FilterRule filterRule : moderateChatInfo.getFilterRules()) {
                if (filterRule instanceof StickerMessageFilterRule) {
                    StickerMessageFilterRule stickerMessageFilterRule = (StickerMessageFilterRule) filterRule;
                    stickerMessageFilterRule.removeExceptUser(userName);
                    log.debug("Removed from exclusions. User: " + userName + "; Chat: " + chatName);
                    botClient.sendMessage(messageDTO.getFrom().getId(),
                            "Removed from exclusions. User: " + userName + "; Chat: " + chatName);
                }
            }
        } catch (IOException | RpcErrorException e) {
            e.printStackTrace();
        }
    }

    private void processEnableModerationCommand(MessageDTO messageDTO) {
        String[] msgParts = messageDTO.getText().split(":");

        if (msgParts.length != 2) {
            log.debug("Not enough command parts. Usage: /addModeratedChat:chat title");
            botClient.sendMessage(messageDTO.getFrom().getId(),
                    "ERROR: Not enough command parts. Usage: /addModeratedChat:chat title");
            return;
        }

        String chatName = msgParts[1];

        try {
            Optional<ChatInfo> chat = mProtoClient.findChat(chatName);
            if (!chat.isPresent()) {
                log.debug("Can't find chat with name: " + chatName);
                botClient.sendMessage(messageDTO.getFrom().getId(),
                        "ERROR: Can't find chat with name: " + chatName);
                return;
            }

            if (!chat.get().isMocheratorAdmin()) {
                log.debug("Mocherator is not an admin in chat: " + chatName);
                botClient.sendMessage(messageDTO.getFrom().getId(),
                        "ERROR: Mocherator is not an admin in chat: " + chatName);
                return;
            }

            if (!chat.get().getBotIds().contains(botClient.getId())) {
                log.debug("@MocheratorBot is not added to the chat: " + chatName);
                botClient.sendMessage(messageDTO.getFrom().getId(),
                        "ERROR: @MocheratorBot is not added to the chat: " + chatName);
                return;
            }

            if (chatsToModerate.containsKey(chat.get().getId())) {
                log.debug("Already in moderation: " + chatName);
                botClient.sendMessage(messageDTO.getFrom().getId(),
                        "ERROR: Already in moderation: " + chatName);
                return;
            }

            chatsToModerate.put(chat.get().getId(), new ModerateChatInfo(chat.get(), new StickerMessageFilterRule()));
            log.debug("Added to moderation: " + chatName);
            botClient.sendMessage(messageDTO.getFrom().getId(),
                    "Added to moderation: " + chatName);
            // botClient.sendMessage(chat.get().getId(), "Moderation is enabled for this chat.");
            // Here should be original id. Investigate how to convert from mtProto id to bot api id.
        } catch (IOException | RpcErrorException e) {
            e.printStackTrace();
        }
    }

    private void processDisableModerationCommand(MessageDTO messageDTO) {
        String[] msgParts = messageDTO.getText().split(":");

        if (msgParts.length != 2) {
            log.debug("Not enough command parts. Usage: /addModeratedChat:chat title");
            botClient.sendMessage(messageDTO.getFrom().getId(),
                    "ERROR: Not enough command parts. Usage: /addModeratedChat:chat title");
            return;
        }

        String chatName = msgParts[1];

        try {
            Optional<ChatInfo> chat = mProtoClient.findChat(chatName);
            if (!chat.isPresent()) {
                log.debug("Can't find chat with name: " + chatName);
                botClient.sendMessage(messageDTO.getFrom().getId(),
                        "ERROR: Can't find chat with name: " + chatName);
                return;
            }

            chatsToModerate.remove(chat.get().getId());
            log.debug("Removed from moderation: " + chatName);
            botClient.sendMessage(messageDTO.getFrom().getId(),
                    "Removed from moderation: " + chatName);
        } catch (IOException | RpcErrorException e) {
            e.printStackTrace();
        }
    }

    private void processListCommand(MessageDTO messageDTO) {
        String moderationListInfo = moderator.buildModerationListInfo();
        botClient.sendMessage(messageDTO.getChat().getId(), moderationListInfo);
    }
}
