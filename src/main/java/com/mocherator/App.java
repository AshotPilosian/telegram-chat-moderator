package com.mocherator;

import com.github.badoualy.telegram.tl.api.TLMessage;
import com.github.badoualy.telegram.tl.exception.RpcErrorException;
import com.mocherator.moderator.Moderator;
import com.mocherator.telegram.client.TelegramBotClient;
import com.mocherator.telegram.client.TelegramMProtoClient;
import com.mocherator.telegram.TelegramService;
import com.mocherator.telegram.data.ChatInfo;
import com.mocherator.telegram.dto.MessageDTO;
import com.mocherator.telegram.dto.UpdateDTO;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.mocherator.util.Constants.Properties.*;

public class App {

    /*public static void main(String[] args) throws IOException, RpcErrorException, InterruptedException {
        if (args.length != 1) throw new IllegalArgumentException();

        String rootDir = args[0];

        Properties properties = new Properties();
        properties.load(new FileInputStream(new File(rootDir, "config.properties")));

        String apiHash = properties.getProperty(API_HASH);
        int apiId = Integer.parseInt(properties.getProperty(API_ID));
        String phoneNumber = properties.getProperty(PHONE_NUMBER);
        int msWait = Integer.parseInt(properties.getProperty(WAIT_INTERVAL_MS));
        int heartbeatInterval = Integer.parseInt(properties.getProperty(HEARTBEAT_INTERVAL_MS));
        String chatName = "Спешка и котлетки";

        System.out.println(chatName);

        BlockingQueue<UpdateDTO> updates = new LinkedBlockingQueue<>();
        TelegramService telegramService = new TelegramService();
        TelegramBotClient telegramBotClient = new TelegramBotClient(
                "328781135:AAEZFjvK8wmg-0cmP60m-AxnZ9sR2esnD_o", updates);
        telegramBotClient.startReceivingUpdates();
        try (TelegramMProtoClient client = new TelegramMProtoClient(rootDir, apiHash, apiId, phoneNumber)) {
            Optional<ChatInfo> chat = client.findChat("Mocherator Test Group");
            if (!chat.isPresent()) {
                System.out.println("Chat: %s doesn't exists.");
                return;
            }
            ChatInfo chatInfo = chat.get();

            List<UpdateDTO> updatesToProcess = new ArrayList<>();
            while (true) {
                System.out.println("Waiting for updates.");
                updatesToProcess.add(updates.take());
                // TODO: Rewrite this shit. May be cause of long check pauses.
                Thread.sleep(300);
                while (!updates.isEmpty() && updatesToProcess.size() < 5) {
                    Thread.sleep(200);
                    UpdateDTO updateDTO = updates.poll();
                    if (updateDTO != null) {
                        updatesToProcess.add(updateDTO);
                    }
                }

                System.out.println("Received updates: " + updatesToProcess.size());
                List<MessageDTO> messagesToRemove = updatesToProcess.stream()
                        .filter(update -> update.getMessage() != null)
                        .filter(update -> update.getMessage().getChat() != null)
                        .filter(update -> chatInfo.getId().equals(update.getMessage().getChat().getId()))
                        .filter(update -> update.getMessage().getSticker() != null)
                        .filter(update -> update.getMessage().getFrom() != null)
                        .filter(update -> update.getMessage().getFrom().getFirstName() != null)
                        // .filter(update -> !update.getMessage().getFrom().getFirstName().equals("Ashot"))
                        .map(UpdateDTO::getMessage)
                        .collect(Collectors.toList());

                System.out.println("Messages to remove: " + messagesToRemove.size());

                client.deleteMessages(messagesToRemove, chatInfo);

                updatesToProcess.clear();
            }

            *//*int lastProcessedMsgId = 0;
            long previousHeartbeatMsg = 0;
            while (true) {
                List<TLMessage> messages = client.loadMessages(chatInfo, lastProcessedMsgId, 100);
                lastProcessedMsgId = telegramService.findMaxMessageId(messages).orElse(lastProcessedMsgId);
                List<TLMessage> messagesWithStickers = telegramService.findMessagesWithStickers(messages);

                if ((!messagesWithStickers.isEmpty()) ||
                        (System.currentTimeMillis() - previousHeartbeatMsg) >= heartbeatInterval){
                    previousHeartbeatMsg = System.currentTimeMillis();
                    System.out.println(buildMessagesInfo(messages, messagesWithStickers));
                }

                if (!messagesWithStickers.isEmpty()) {
                    client.deleteMessages(messagesWithStickers, chatInfo);
                }

                Thread.sleep(msWait);
            }*//*
        }
    }*/

    public static void main(String[] args) throws IOException, RpcErrorException, InterruptedException {
        if (args.length != 1) throw new IllegalArgumentException();

        String rootDir = args[0];

        Properties properties = new Properties();
        properties.load(new FileInputStream(new File(rootDir, "config.properties")));

        String apiHash = properties.getProperty(API_HASH);
        int apiId = Integer.parseInt(properties.getProperty(API_ID));
        String phoneNumber = properties.getProperty(PHONE_NUMBER);
        String botToken = properties.getProperty(BOT_TOKEN);
        int msWait = Integer.parseInt(properties.getProperty(WAIT_INTERVAL_MS));
        int heartbeatInterval = Integer.parseInt(properties.getProperty(HEARTBEAT_INTERVAL_MS));

        Moderator moderator = new Moderator(rootDir, apiHash, apiId, phoneNumber, botToken);
        moderator.loadModerationConfig();
        moderator.start();
    }

    private static String buildMessagesInfo(List<TLMessage> messages, List<TLMessage> messagesWithStickers) {
        return String.format("Date: %s. Number of messages: %s. Messages with stickers: %s",
                new Date(),
                messages.size(),
                messagesWithStickers.size());
    }
}
