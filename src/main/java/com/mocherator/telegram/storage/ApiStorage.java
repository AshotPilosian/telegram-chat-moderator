package com.mocherator.telegram.storage;

import com.github.badoualy.telegram.api.TelegramApiStorage;
import com.github.badoualy.telegram.mtproto.DataCenter;
import com.github.badoualy.telegram.mtproto.auth.AuthKey;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ApiStorage implements TelegramApiStorage {
    private final File authKeyFile;
    private final File nearestDCFile;
    private final File saltFile;

    public ApiStorage(String rootDir) {
        this.authKeyFile = new File(rootDir, "auth.key");
        this.nearestDCFile = new File(rootDir, "dc.save");
        this.saltFile = new File(rootDir, "salt.save");
    }

    public void saveAuthKey(@NotNull AuthKey authKey) {
        try {
            FileUtils.writeByteArrayToFile(authKeyFile, authKey.getKey());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    public AuthKey loadAuthKey() {
        try {
            return new AuthKey(FileUtils.readFileToByteArray(authKeyFile));
        } catch (IOException e) {
            if (!(e instanceof FileNotFoundException))
                e.printStackTrace();
        }

        return null;
    }

    public void saveDc(@NotNull DataCenter dataCenter) {
        try {
            FileUtils.write(nearestDCFile, dataCenter.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    public DataCenter loadDc() {
        try {
            String[] infos = FileUtils.readFileToString(nearestDCFile).split(":");
            return new DataCenter(infos[0], Integer.parseInt(infos[1]));
        } catch (IOException e) {
            if (!(e instanceof FileNotFoundException))
                e.printStackTrace();
        }

        return null;
    }

    public void deleteAuthKey() {
        try {
            FileUtils.forceDelete(authKeyFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteDc() {
        try {
            FileUtils.forceDelete(nearestDCFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveServerSalt(long salt) {
        try {
            FileUtils.writeStringToFile(saltFile, String.valueOf(salt));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    public Long loadServerSalt() {
        try {
            return Long.parseLong(FileUtils.readFileToString(saltFile));
        } catch (IOException e) {
            if (!(e instanceof FileNotFoundException))
                e.printStackTrace();
        }

        return null;
    }
}