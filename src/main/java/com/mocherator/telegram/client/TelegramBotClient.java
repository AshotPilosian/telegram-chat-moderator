package com.mocherator.telegram.client;


import com.mocherator.telegram.dto.*;
import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.TelegramBotAdapter;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.model.*;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.SendResponse;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

public class TelegramBotClient {
    private final String token;
    private final int id;
    private final BlockingQueue<UpdateDTO> updatesQueue;
    private final TelegramBot bot;

    public TelegramBotClient(String token) {
        this(token, new LinkedBlockingQueue<>());
    }

    public TelegramBotClient(String token, BlockingQueue<UpdateDTO> updatesQueue) {
        this.token = token;
        this.id = Integer.valueOf(token.split(":")[0]);
        this.updatesQueue = updatesQueue;
        this.bot = TelegramBotAdapter.build(token);
    }

    public void startReceivingUpdates() {
        bot.setUpdatesListener(updates -> {
            updatesQueue.addAll(updates.stream()
                    .map(TelegramBotClient::convertToDTO)
                    .collect(Collectors.toList())
            );

            return UpdatesListener.CONFIRMED_UPDATES_ALL;
        });
    }

    public void stopReceivingUpdates() {
        bot.removeGetUpdatesListener();
    }

    public void sendMessage(Integer chatId, String text) {
        SendResponse execute = bot.execute(new SendMessage(chatId, text));
    }

    public BlockingQueue<UpdateDTO> getUpdatesQueue() {
        return updatesQueue;
    }

    public int getId() {
        return id;
    }

    private static MessageDTO convertToDTO(Message message) {
        if (message == null) {
            return null;
        }

        return new MessageDTO(
                message.messageId(),
                convertToDTO(message.from()),
                message.date(),
                convertToDTO(message.chat()),
                convertToDTO(message.forwardFrom()),
                convertToDTO(message.forwardFromChat()),
                message.forwardFromMessageId(),
                message.forwardDate(),
                convertToDTO(message.replyToMessage()),
                message.editDate(),
                message.text(),
                convertToDTO(message.sticker()),
                message.caption(),
                convertToDTO(message.newChatMember()),
                convertToDTO(message.leftChatMember()),
                message.newChatTitle(),
                message.deleteChatPhoto(),
                message.groupChatCreated(),
                message.supergroupChatCreated(),
                message.channelChatCreated(),
                message.migrateToChatId(),
                message.migrateFromChatId(),
                convertToDTO(message.pinnedMessage())
        );
    }

    private static ChatDTO convertToDTO(Chat chat) {
        if (chat == null) {
            return null;
        }

        return new ChatDTO(
                chat.id(),
                normalizeId(chat.id()),
                chat.type().name(),
                chat.firstName(),
                chat.lastName(),
                chat.username(),
                chat.title(),
                chat.allMembersAreAdministrators()
        );
    }

    private static StickerDTO convertToDTO(Sticker sticker) {
        if (sticker == null) {
            return null;
        }

        return new StickerDTO(
                sticker.fileId(),
                sticker.width(),
                sticker.height(),
                sticker.emoji(),
                sticker.fileSize()
        );
    }

    private static UpdateDTO convertToDTO(Update update) {
        if (update == null) {
            return null;
        }

        return new UpdateDTO(
                update.updateId(),
                convertToDTO(update.message()),
                convertToDTO(update.editedMessage()),
                convertToDTO(update.channelPost()),
                convertToDTO(update.editedChannelPost())
        );
    }

    private static UserDTO convertToDTO(User user) {
        if (user == null) {
            return null;
        }

        return new UserDTO(
                user.id(),
                user.firstName(),
                user.lastName(),
                user.username()
        );
    }

    // TODO: Huge spike here. Need to read about big endian/little endian stuff and make tests.
    private static int normalizeId(Long id){
        long unsignedId = Math.abs(id);
        int normalizedId;
        if (unsignedId > 1000000000000L) {
            normalizedId = (int) (unsignedId - 1000000000000L);
        } else {
            normalizedId = (int) unsignedId;
        }

        return normalizedId;
    }
}
