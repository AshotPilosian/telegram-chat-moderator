package com.mocherator.telegram.client;

import com.github.badoualy.telegram.api.Kotlogram;
import com.github.badoualy.telegram.api.TelegramApp;
import com.github.badoualy.telegram.tl.api.*;
import com.github.badoualy.telegram.tl.api.auth.TLAbsSentCode;
import com.github.badoualy.telegram.tl.api.auth.TLAuthorization;
import com.github.badoualy.telegram.tl.api.messages.*;
import com.github.badoualy.telegram.tl.api.messages.TLChatFull;
import com.github.badoualy.telegram.tl.core.TLIntVector;
import com.github.badoualy.telegram.tl.exception.RpcErrorException;
import com.mocherator.telegram.data.ChatInfo;
import com.mocherator.telegram.dto.MessageDTO;
import com.mocherator.telegram.storage.ApiStorage;
import one.util.streamex.StreamEx;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

public class TelegramMProtoClient implements AutoCloseable {
    private static final String APP_VERSION = "AppVersion";
    private static final String MODEL = "Model";
    private static final String SYSTEM_VERSION = "SysVer";
    private static final String LANG_CODE = "en";

    private final com.github.badoualy.telegram.api.TelegramClient telegramClient;

    public TelegramMProtoClient(String rootDir, String apiHash, int apiId, String phoneNumber) {
        TelegramApp application = new TelegramApp(apiId, apiHash, MODEL, SYSTEM_VERSION, APP_VERSION, LANG_CODE);
        ApiStorage apiStorage = new ApiStorage(rootDir);

        com.github.badoualy.telegram.api.TelegramClient client = Kotlogram.getDefaultClient(application, apiStorage);
        if (apiStorage.loadServerSalt() == null) {
            auth(client, phoneNumber);
        }

        this.telegramClient = client;
    }

    private void auth(com.github.badoualy.telegram.api.TelegramClient client, String phoneNumber) {
        try {
            TLAbsSentCode sentCode = client.authSendCode(phoneNumber, 5);
            System.out.println("Authentication code: ");

            String code = new Scanner(System.in).nextLine();
            String codeHash = sentCode.getPhoneCodeHash();

            System.out.println(codeHash);

            TLAuthorization authorization = client.authSignIn(phoneNumber, codeHash, code);
            TLUser self = authorization.getUser().getAsUser();
            System.out.println("You are now signed in as " + self.getFirstName() + " " + self.getLastName() +
                    " @" + self.getUsername());
        } catch (Exception e) {
            throw new RuntimeException("Can't auth in Telegram.", e);
        }
    }

    public List<TLMessage> loadMessages(ChatInfo chat, int minMessageId, int msgCount)
            throws IOException, RpcErrorException {
        TLAbsMessages messages = telegramClient.messagesGetHistory(
                new TLInputPeerChannel(chat.getId(), chat.getAccessHash()), 0, 0, msgCount, 0, minMessageId);
        return StreamEx.of(messages.getMessages())
                .select(TLMessage.class)
                .collect(Collectors.toList());
    }

    public Optional<ChatInfo> findChat(String name) throws IOException, RpcErrorException {
        TLAbsDialogs dialogs = telegramClient.messagesGetDialogs(0, 0, new TLInputPeerEmpty(), 1000);
        return StreamEx.of(dialogs.getChats())
                .map(this::createChatInfo)
                .filter(chat -> name.equals(chat.getTitle()))
                .findFirst();
    }

    public TLAffectedMessages deleteMessages(List<MessageDTO> messages, ChatInfo chat)
            throws IOException, RpcErrorException {
        List<Integer> messageIds = messages.stream()
                .map(MessageDTO::getMessageId)
                .collect(Collectors.toList());

        TLIntVector msgToDelete = new TLIntVector();
        msgToDelete.addAll(messageIds);

        return telegramClient.channelsDeleteMessages(
                new TLInputChannel(chat.getId(), chat.getAccessHash()), msgToDelete);
    }

    @Override
    public void close() {
        telegramClient.close();
    }

    private ChatInfo createChatInfo(TLAbsChat absChat) {
        int id = absChat.getId();

        String title = null;
        boolean isMocheratorAdmin = false;
        Long accessHash = null;
        List<Integer> botIds = new ArrayList<>();
        try {
            if (absChat instanceof TLChannel) {
                TLChannel channel = (TLChannel) absChat;
                title = channel.getTitle();
                isMocheratorAdmin = channel.getCreator() || channel.getModerator() || channel.getEditor();
                accessHash = channel.getAccessHash();

                TLChatFull tlChatFull = telegramClient.channelsGetFullChannel(
                        new TLInputChannel(absChat.getId(), ((TLChannel) absChat).getAccessHash()));
                botIds = tlChatFull.getFullChat().getBotInfo().stream()
                        .map(TLAbsBotInfo::getAsBotInfo)
                        .map(TLBotInfo::getUserId)
                        .collect(Collectors.toList());
            } else if (absChat instanceof TLChat) {
                TLChat chat = (TLChat) absChat;
                title = chat.getTitle();
                isMocheratorAdmin = chat.getCreator() || chat.getAdmin();
            }
        } catch (IOException | RpcErrorException e) {
            e.printStackTrace();
        }

        return new ChatInfo(id, accessHash, title, isMocheratorAdmin, botIds);
    }
}
