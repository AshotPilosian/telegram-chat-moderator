package com.mocherator.telegram.dto;

import java.util.Objects;

public class StickerDTO {
    private final String fileId;
    private final Integer width;
    private final Integer height;
    private final String emoji;
    private final Integer fileSize;

    // private PhotoSize thumb;

    public StickerDTO(String fileId, Integer width, Integer height, String emoji, Integer fileSize) {
        this.fileId = fileId;
        this.width = width;
        this.height = height;
        this.emoji = emoji;
        this.fileSize = fileSize;
    }

    public String getFileId() {
        return fileId;
    }

    public Integer getWidth() {
        return width;
    }

    public Integer getHeight() {
        return height;
    }

    public String getEmoji() {
        return emoji;
    }

    public Integer getFileSize() {
        return fileSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StickerDTO that = (StickerDTO) o;
        return Objects.equals(fileId, that.fileId) &&
                Objects.equals(width, that.width) &&
                Objects.equals(height, that.height) &&
                Objects.equals(emoji, that.emoji) &&
                Objects.equals(fileSize, that.fileSize);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fileId, width, height, emoji, fileSize);
    }

    @Override
    public String toString() {
        return "StickerDTO{" +
                "fileId='" + fileId + '\'' +
                ", width=" + width +
                ", height=" + height +
                ", emoji='" + emoji + '\'' +
                ", fileSize=" + fileSize +
                '}';
    }
}
