package com.mocherator.telegram.dto;

import java.util.Objects;

public class MessageDTO {
    private final Integer messageId;
    private final UserDTO from;
    private final Integer date;
    private final ChatDTO chat;
    private final UserDTO forwardFrom;
    private final ChatDTO forwardFromChat;
    private final Integer forwardFromMessageId;
    private final Integer forwardDate;
    private final MessageDTO replyToMessage;
    private final Integer editDate;
    private final String text;
    private final StickerDTO sticker;
    private final String caption;
    private final UserDTO newChatMember;
    private final UserDTO leftChatMember;
    private final String newChatTitle;
    private final Boolean deleteChatPhoto;
    private final Boolean groupChatCreated;
    private final Boolean supergroupChatCreated;
    private final Boolean channelChatCreated;
    private final Long migrateToChatId;
    private final Long migrateFromChatId;
    private final MessageDTO pinnedMessage;

    // private Video video;
    // private Voice voice;
    // private PhotoSize[] new_chat_photo;
    // private Contact contact;
    // private Location location;
    // private Venue venue;
    // private MessageEntity[] entities;
    // private Audio audio;
    // private Document document;
    // private Game game;
    // private PhotoSize[] photo;

    public MessageDTO(Integer messageId, UserDTO from, Integer date, ChatDTO chat, UserDTO forwardFrom,
                      ChatDTO forwardFromChat, Integer forwardFromMessageId, Integer forwardDate,
                      MessageDTO replyToMessage, Integer editDate, String text, StickerDTO sticker, String caption,
                      UserDTO newChatMember, UserDTO leftChatMember, String newChatTitle, Boolean deleteChatPhoto,
                      Boolean groupChatCreated, Boolean supergroupChatCreated, Boolean channelChatCreated,
                      Long migrateToChatId, Long migrateFromChatId, MessageDTO pinnedMessage) {
        this.messageId = messageId;
        this.from = from;
        this.date = date;
        this.chat = chat;
        this.forwardFrom = forwardFrom;
        this.forwardFromChat = forwardFromChat;
        this.forwardFromMessageId = forwardFromMessageId;
        this.forwardDate = forwardDate;
        this.replyToMessage = replyToMessage;
        this.editDate = editDate;
        this.text = text;
        this.sticker = sticker;
        this.caption = caption;
        this.newChatMember = newChatMember;
        this.leftChatMember = leftChatMember;
        this.newChatTitle = newChatTitle;
        this.deleteChatPhoto = deleteChatPhoto;
        this.groupChatCreated = groupChatCreated;
        this.supergroupChatCreated = supergroupChatCreated;
        this.channelChatCreated = channelChatCreated;
        this.migrateToChatId = migrateToChatId;
        this.migrateFromChatId = migrateFromChatId;
        this.pinnedMessage = pinnedMessage;
    }

    public Integer getMessageId() {
        return messageId;
    }

    public UserDTO getFrom() {
        return from;
    }

    public Integer getDate() {
        return date;
    }

    public ChatDTO getChat() {
        return chat;
    }

    public UserDTO getForwardFrom() {
        return forwardFrom;
    }

    public ChatDTO getForwardFromChat() {
        return forwardFromChat;
    }

    public Integer getForwardFromMessageId() {
        return forwardFromMessageId;
    }

    public Integer getForwardDate() {
        return forwardDate;
    }

    public MessageDTO getReplyToMessage() {
        return replyToMessage;
    }

    public Integer getEditDate() {
        return editDate;
    }

    public String getText() {
        return text;
    }

    public StickerDTO getSticker() {
        return sticker;
    }

    public String getCaption() {
        return caption;
    }

    public UserDTO getNewChatMember() {
        return newChatMember;
    }

    public UserDTO getLeftChatMember() {
        return leftChatMember;
    }

    public String getNewChatTitle() {
        return newChatTitle;
    }

    public Boolean getDeleteChatPhoto() {
        return deleteChatPhoto;
    }

    public Boolean getGroupChatCreated() {
        return groupChatCreated;
    }

    public Boolean getSupergroupChatCreated() {
        return supergroupChatCreated;
    }

    public Boolean getChannelChatCreated() {
        return channelChatCreated;
    }

    public Long getMigrateToChatId() {
        return migrateToChatId;
    }

    public Long getMigrateFromChatId() {
        return migrateFromChatId;
    }

    public MessageDTO getPinnedMessage() {
        return pinnedMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageDTO that = (MessageDTO) o;
        return Objects.equals(messageId, that.messageId) &&
                Objects.equals(from, that.from) &&
                Objects.equals(date, that.date) &&
                Objects.equals(chat, that.chat) &&
                Objects.equals(forwardFrom, that.forwardFrom) &&
                Objects.equals(forwardFromChat, that.forwardFromChat) &&
                Objects.equals(forwardFromMessageId, that.forwardFromMessageId) &&
                Objects.equals(forwardDate, that.forwardDate) &&
                Objects.equals(replyToMessage, that.replyToMessage) &&
                Objects.equals(editDate, that.editDate) &&
                Objects.equals(text, that.text) &&
                Objects.equals(sticker, that.sticker) &&
                Objects.equals(caption, that.caption) &&
                Objects.equals(newChatMember, that.newChatMember) &&
                Objects.equals(leftChatMember, that.leftChatMember) &&
                Objects.equals(newChatTitle, that.newChatTitle) &&
                Objects.equals(deleteChatPhoto, that.deleteChatPhoto) &&
                Objects.equals(groupChatCreated, that.groupChatCreated) &&
                Objects.equals(supergroupChatCreated, that.supergroupChatCreated) &&
                Objects.equals(channelChatCreated, that.channelChatCreated) &&
                Objects.equals(migrateToChatId, that.migrateToChatId) &&
                Objects.equals(migrateFromChatId, that.migrateFromChatId) &&
                Objects.equals(pinnedMessage, that.pinnedMessage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(messageId, from, date, chat, forwardFrom, forwardFromChat, forwardFromMessageId,
                forwardDate, replyToMessage, editDate, text, sticker, caption, newChatMember, leftChatMember,
                newChatTitle, deleteChatPhoto, groupChatCreated, supergroupChatCreated, channelChatCreated,
                migrateToChatId, migrateFromChatId, pinnedMessage);
    }

    @Override
    public String toString() {
        return "MessageDTO{" +
                "messageId=" + messageId +
                ", from=" + from +
                ", date=" + date +
                ", chat=" + chat +
                ", forwardFrom=" + forwardFrom +
                ", forwardFromChat=" + forwardFromChat +
                ", forwardFromMessageId=" + forwardFromMessageId +
                ", forwardDate=" + forwardDate +
                ", replyToMessage=" + replyToMessage +
                ", editDate=" + editDate +
                ", text='" + text + '\'' +
                ", sticker=" + sticker +
                ", caption='" + caption + '\'' +
                ", newChatMember=" + newChatMember +
                ", leftChatMember=" + leftChatMember +
                ", newChatTitle='" + newChatTitle + '\'' +
                ", deleteChatPhoto=" + deleteChatPhoto +
                ", groupChatCreated=" + groupChatCreated +
                ", supergroupChatCreated=" + supergroupChatCreated +
                ", channelChatCreated=" + channelChatCreated +
                ", migrateToChatId=" + migrateToChatId +
                ", migrateFromChatId=" + migrateFromChatId +
                ", pinnedMessage=" + pinnedMessage +
                '}';
    }
}
