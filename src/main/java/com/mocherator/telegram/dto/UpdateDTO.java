package com.mocherator.telegram.dto;

import java.util.Objects;

public class UpdateDTO {
    private final Integer updateId;
    private final MessageDTO message;
    private final MessageDTO editedMessage;
    private final MessageDTO channelPost;
    private final MessageDTO editedChannelPost;

    // private InlineQuery inline_query;
    // private ChosenInlineResult chosen_inline_result;
    // private CallbackQuery callback_query;

    public UpdateDTO(Integer updateId, MessageDTO message, MessageDTO editedMessage, MessageDTO channelPost, MessageDTO editedChannelPost) {
        this.updateId = updateId;
        this.message = message;
        this.editedMessage = editedMessage;
        this.channelPost = channelPost;
        this.editedChannelPost = editedChannelPost;
    }

    public Integer getUpdateId() {
        return updateId;
    }

    public MessageDTO getMessage() {
        return message;
    }

    public MessageDTO getEditedMessage() {
        return editedMessage;
    }

    public MessageDTO getChannelPost() {
        return channelPost;
    }

    public MessageDTO getEditedChannelPost() {
        return editedChannelPost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UpdateDTO updateDTO = (UpdateDTO) o;
        return Objects.equals(updateId, updateDTO.updateId) &&
                Objects.equals(message, updateDTO.message) &&
                Objects.equals(editedMessage, updateDTO.editedMessage) &&
                Objects.equals(channelPost, updateDTO.channelPost) &&
                Objects.equals(editedChannelPost, updateDTO.editedChannelPost);
    }

    @Override
    public int hashCode() {
        return Objects.hash(updateId, message, editedMessage, channelPost, editedChannelPost);
    }

    @Override
    public String toString() {
        return "UpdateDTO{" +
                "updateId=" + updateId +
                ", message=" + message +
                ", editedMessage=" + editedMessage +
                ", channelPost=" + channelPost +
                ", editedChannelPost=" + editedChannelPost +
                '}';
    }
}
