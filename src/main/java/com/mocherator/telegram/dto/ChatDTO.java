package com.mocherator.telegram.dto;

import java.util.Objects;

public class ChatDTO {
    private final Long originalId;
    private final Integer id;
    private final String type;

    //Private
    private final String firstName;
    private final String lastName;

    //Private and Channel
    private final String username;

    //Channel and Group
    private final String title;

    private final Boolean allMembersAreAdministrators;

    public ChatDTO(Long originalId, Integer id, String type, String firstName, String lastName, String username,
                   String title, Boolean allMembersAreAdministrators) {
        this.originalId = originalId;
        this.id = id;
        this.type = type;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.title = title;
        this.allMembersAreAdministrators = allMembersAreAdministrators;
    }

    public Long getOriginalId() {
        return originalId;
    }

    public Integer getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getUsername() {
        return username;
    }

    public String getTitle() {
        return title;
    }

    public Boolean getAllMembersAreAdministrators() {
        return allMembersAreAdministrators;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChatDTO chatDTO = (ChatDTO) o;
        return Objects.equals(originalId, chatDTO.originalId) &&
                Objects.equals(id, chatDTO.id) &&
                type == chatDTO.type &&
                Objects.equals(firstName, chatDTO.firstName) &&
                Objects.equals(lastName, chatDTO.lastName) &&
                Objects.equals(username, chatDTO.username) &&
                Objects.equals(title, chatDTO.title) &&
                Objects.equals(allMembersAreAdministrators, chatDTO.allMembersAreAdministrators);
    }

    @Override
    public int hashCode() {
        return Objects.hash(originalId, id, type, firstName, lastName, username, title, allMembersAreAdministrators);
    }

    @Override
    public String toString() {
        return "ChatDTO{" +
                "originalId=" + originalId +
                ", id=" + id +
                ", type=" + type +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", username='" + username + '\'' +
                ", title='" + title + '\'' +
                ", allMembersAreAdministrators=" + allMembersAreAdministrators +
                '}';
    }
}
