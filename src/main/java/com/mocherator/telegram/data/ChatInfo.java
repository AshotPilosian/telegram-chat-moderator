package com.mocherator.telegram.data;

import java.util.List;
import java.util.Objects;

public class ChatInfo {
    private final Integer id;
    private final Long accessHash;
    private final String title;
    private final boolean isMocheratorAdmin;
    private final List<Integer> botIds;

    public ChatInfo(Integer id, Long accessHash, String title, boolean isMocheratorAdmin, List<Integer> botIds) {
        this.id = id;
        this.accessHash = accessHash;
        this.title = title;
        this.isMocheratorAdmin = isMocheratorAdmin;
        this.botIds = botIds;
    }

    public Integer getId() {
        return id;
    }

    public Long getAccessHash() {
        return accessHash;
    }

    public String getTitle() {
        return title;
    }

    public boolean isMocheratorAdmin() {
        return isMocheratorAdmin;
    }

    public List<Integer> getBotIds() {
        return botIds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChatInfo chatInfo = (ChatInfo) o;
        return Objects.equals(id, chatInfo.id) &&
                Objects.equals(accessHash, chatInfo.accessHash);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accessHash);
    }

    @Override
    public String toString() {
        return "ChatInfo{" +
                "id=" + id +
                ", accessHash=" + accessHash +
                ", title='" + title + '\'' +
                ", isMocheratorAdmin=" + isMocheratorAdmin +
                ", botIds=" + botIds +
                '}';
    }
}