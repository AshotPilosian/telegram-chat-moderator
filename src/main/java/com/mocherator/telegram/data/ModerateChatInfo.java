package com.mocherator.telegram.data;

import com.mocherator.moderator.filter.FilterRule;

import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Ashot on 30/12/2016.
 */
public class ModerateChatInfo {
    private final ChatInfo chatInfo;
    private final CopyOnWriteArrayList<FilterRule> filterRules;

    public ModerateChatInfo(ChatInfo chatInfo, FilterRule... filterRules) {
        this(chatInfo);
        for (FilterRule rule : filterRules) {
            this.filterRules.add(rule);
        }
    }

    public ModerateChatInfo(ChatInfo chatInfo) {
        this.chatInfo = chatInfo;
        this.filterRules = new CopyOnWriteArrayList<>();
    }

    public ChatInfo getChatInfo() {
        return chatInfo;
    }

    public CopyOnWriteArrayList<FilterRule> getFilterRules() {
        return filterRules;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModerateChatInfo that = (ModerateChatInfo) o;
        return Objects.equals(chatInfo, that.chatInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(chatInfo);
    }

    @Override
    public String toString() {
        return "ModerateChatInfo{" +
                "chatInfo=" + chatInfo +
                ", filterRules=" + filterRules +
                '}';
    }
}
