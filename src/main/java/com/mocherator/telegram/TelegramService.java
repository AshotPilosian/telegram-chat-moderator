package com.mocherator.telegram;

import com.github.badoualy.telegram.tl.api.TLAbsMessage;
import com.github.badoualy.telegram.tl.api.TLDocumentAttributeSticker;
import com.github.badoualy.telegram.tl.api.TLMessage;
import com.github.badoualy.telegram.tl.api.TLMessageMediaDocument;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TelegramService {
    public Optional<Integer> findMaxMessageId(List<TLMessage> messages) {
        return messages.stream().map(TLAbsMessage::getId).sorted(Comparator.reverseOrder()).findFirst();
    }

    public List<TLMessage> findMessagesWithStickers(List<TLMessage> messages) {
        return messages.stream()
                .filter(msg -> msg.getMedia() instanceof TLMessageMediaDocument)
                .filter(msg -> ((TLMessageMediaDocument) msg.getMedia())
                        .getDocument().getAsDocument().getAttributes().stream()
                        .anyMatch(attr -> attr instanceof TLDocumentAttributeSticker))
                .collect(Collectors.toList());
    }
}
