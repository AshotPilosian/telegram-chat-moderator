package com.mocherator.util;

public interface Constants {
    interface Properties {
        String API_ID = "API_ID";
        String API_HASH = "API_HASH";
        String PHONE_NUMBER = "PHONE_NUMBER";
        String BOT_TOKEN = "BOT_TOKEN";
        String HEARTBEAT_INTERVAL_MS = "HEARTBEAT_INTERVAL_MS";
        String WAIT_INTERVAL_MS = "WAIT_INTERVAL_MS";
        String CHAT_NAME = "CHAT_NAME";
    }
}
